# Package

version       = "0.1.0"
author        = "esbraff"
description   = "Nim Implemented Assembler"
license       = "Proprietary"
srcDir        = "src"
bin           = @["nia"]


# Dependencies

requires "nim >= 0.19.4"
