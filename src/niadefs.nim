template mp*(vm): float = vm.memory[0] # Memory pointer register.
template mc*(vm): float = vm.memory[int(mp(vm))] # Value pointed to by the memory pointer.
template sp*(vm): float = vm.memory[1] # Stack pointer register.
template sc*(vm): float = vm.memory[int(sp(vm))] # Value pointed to by the stack pointer.
template so*(vm; offset: int32): float = vm.memory[int(sp(vm)) - offset] # Get value from stack at specified offset.