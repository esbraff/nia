from niatypes import NIADEBUG, NIABREAKPOINT, NIATAREA
from niaopcodes import OP_LAST
from niautils import box

# ehco moves line itself, so I'd prefer using printf instead.
# Also I'm sure printf is more efficient.
proc printf(formatstr: cstring) {.header: "<stdio.h>", varargs.}

# I heard that readLine is pretty slow, so I use getchar.
proc getchar(): cint {.header: "<stdio.h>", varargs.}

# Debug text for each opcode.
const DEBUG_OPCODES: array[0..OP_LAST, string] = [
  "NOP",
  "PUSH <value>",
  "POP",
  "<value> DUP",
  "SWAP <a, b>",
  "<a, b> ADD",
  "<a, b> SUB",
  "<a, b> MUL",
  "<a, b> DIV",
  "<a, b> POW",
  "<value> INCR",
  "<value> DECR",
  "JMP <pos>",
  "<a, b> JNE <pos>",
  "<value> PRINT",
  "EXIT <code>"
]

proc newNIADEBUG*(): NIADEBUG =
  NIADEBUG(breakpoints_head: nil, tareas_head: nil)

proc newNIABREAKPOINT*(pos: int; next: ref NIABREAKPOINT; name: string): NIABREAKPOINT =
  NIABREAKPOINT(pos: pos, next: next, name: name)

proc newNIATAREA*(area: HSlice[int, int]; next: ref NIATAREA; name: string): NIATAREA =
  NIATAREA(area: area, next: next, name: name)

proc draw(label: string; memory: seq[float]; area: HSlice[int, int]; max, pos: int) =
  ## Draws specified area in the memory to the console.
  ## label - Label given to the specified area.
  ## memory - Debugged VM memory.
  ## area - Debugged area bounds.
  ## max - The maximum position that can be reached. Most often it's VM memory size.
  ## pos - The current value of the memory pointer.

  echo label, ":"

  for i in countup(area.a, area.b):
    if i >= max:
      return

    if i == pos:
      printf("> > > > ")
    else:
      printf("\t")

    var n = memory[i]
    var ni = int(n)
    printf("[%ld]\t%lf", i, n)

    if ni <= OP_LAST:
      printf("\t\t%s", DEBUG_OPCODES[ni])

    printf("\n")
  printf("\n")

method draw_breakpoints(this: ref NIADEBUG; memory: seq[float]; pos: int) {.base.} =
  ## Draws all the breakpoints to the console.
  ## memory - Debugged VM memory.
  ## pos - The current value of the memory pointer.

  var next = this.breakpoints_head

  while not is_nil(next):
    var breakpoint = next
    draw(breakpoint.name, memory, breakpoint.pos..breakpoint.pos, high(int), pos)

    next = breakpoint.next

method draw_tareas(this: ref NIADEBUG; memory: seq[float]; pos: int) {.base.} =
  ## Draws all the tracking areas to the console.
  ## memory - Debugged VM memory.
  ## pos - The current value of the memory pointer.

  var next = this.tareas_head

  while not is_nil(next):
    var tarea = next
    draw(tarea.name, memory, tarea.area, high(int), pos)

    next = tarea.next

method add_breakpoint*(this: ref NIADEBUG; breakpoint: ref NIABREAKPOINT) {.base.} =
  ## Adds new breakpoint.
  ## breakpointref - Pointer to the breakpoint to be added.

  breakpoint.next = this.breakpoints_head
  this.breakpoints_head = breakpoint

method add_tarea*(this: ref NIADEBUG; tarea: ref NIATAREA) {.base.} =
  ## Adds new tracking area.
  ## tarearef - Pointer to the tracking area to be added.

  tarea.next = this.tareas_head
  this.tareas_head = tarea

method add_breakpoint*(this: ref NIADEBUG; pos: int; name: string) {.base.} =
  ## Constructs and adds new breakpoint.
  ## pos - Position of the breakpoint.
  ## name - Name of the breakpoint.

  var breakpoint = newNIABREAKPOINT(pos, this.breakpoints_head, name)
  this.breakpoints_head = box(breakpoint)

method add_tarea*(this: ref NIADEBUG; area: HSlice[int, int]; name: string) {.base.} =
  ## Constructs and adds new tracking area.
  ## area - Bounds of the tracking area.
  ## name - Name of the tracking area.

  var tarea = newNIATAREA(area, this.tareas_head, name)
  this.tareas_head = box(tarea)

method breakpoint_exists(this: ref NIADEBUG; pos: int): bool {.base.} =
  ## Looks for a breakpoint with the specified position
  ## if it was found, returns true, otherwise false.
  ## pos - Breakpoint position.

  var next = this.breakpoints_head

  while not is_nil(next):
    if next.pos == pos:
      return true

    next = next.next

method run*(this: ref NIADEBUG; memory: seq[float]; pos: int) {.base.} =
  ## Starts the debugging process.
  ## memory - Debugged VM memory.
  ## pos - The current value of the memory pointer.

  if this.breakpoint_exists(pos):
    this.draw_tareas(memory, pos)
    discard getchar()