from niatypes import NIAVM, NIADEBUG, UnknownOperationException
from niadebug import newNIADEBUG, run
from sequtils import concat
from niautils import box
from math import pow
import niaopcodes
import niadefs

proc newNIAVM*(bcode: seq[float]; debug: bool): NIAVM =
  ## Constructs new NIA virtual machine.
  ## bcode - Compiled bytecode.

  const REG_COUNT = 2

  # bcode[0] - Stack size.
  # bcode[1] - Memory size.

  var memory: seq[float] = concat(
    new_seq[float](REG_COUNT + int(bcode[0])), # Registers and stack.
    bcode[2..<bcode.len], # Bytecode without stack size and memory size.
    new_seq[float](int(bcode[1])), # Real memory.
  )

  memory[0] = REG_COUNT + bcode[0] # Memory pointer register.
  memory[1] = REG_COUNT # Stack pointer register.

  var debugobj = newNIADEBUG()

  return NIAVM(
    memory: memory,
    result: 0,
    debug: if debug: box(debugobj)
           else: nil
  )

method interpret*(this: var NIAVM) {.base.} =
  ## Interprets bytecode lying in the NIA virtual machine memory.
  ## Runs debug if it's enabled.

  while (true):
    if not is_nil(this.debug):
      this.debug.run(this.memory, int(mp(this)))

    let op = mc(this)
    mp(this) += 1

    case op:
    of OP_NOP: discard
    # Stack.
    of OP_PUSH:
      sc(this) = mc(this)
      sp(this) += 1
      mp(this) += 1
    of OP_POP:
      sp(this) -= 1
    of OP_DUP:
      sc(this) = so(this, 1)
      sp(this) += 1
    of OP_SWAP:
      let buffer = so(this, 1)
      so(this, 1) = so(this, 2)
      so(this, 2) = buffer
    # Math.
    of OP_ADD:
      let sum = so(this, 2) + so(this, 1)
      sp(this) -= 1
      so(this, 1) = sum
    of OP_SUB:
      let dif = so(this, 2) - so(this, 1)
      sp(this) -= 1
      so(this, 1) = dif
    of OP_MUL:
      let prod = so(this, 2) * so(this, 1)
      sp(this) -= 1
      so(this, 1) = prod
    of OP_DIV:
      let priv = so(this, 2) / so(this, 1)
      sp(this) -= 1
      so(this, 1) = priv
    of OP_POW:
      let result = pow(so(this, 2), so(this, 1))
      sp(this) -= 1
      so(this, 1) = result
    of OP_INCR:
      so(this, 1) += 1
    of OP_DECR:
      so(this, 1) -= 1
    # Special.
    of OP_JMP:
      mp(this) = mc(this)
    of OP_JNE:
      if so(this, 1) != so(this, 2):
        mp(this) = mc(this) - 1
      mp(this) += 1
      sp(this) -= 2
    of OP_PRINT:
      echo $so(this, 1)
      sp(this) -= 1
    of OP_EXIT:
      this.result = int(mc(this))
      break
    else:
      raise new_exception(UnknownOperationException, "Unknown operation code: " & $op & ".")