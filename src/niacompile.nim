from niatypes import UnknownTokenException
from strutils import parse_float
from re import find_all, re
import niaopcodes

proc tokenize*(code: string): seq[string] =
  ## Runs lexical analysis for a given code.
  ## code - Raw code.

  let pattern = re"\S+"
  return code.find_all(pattern)

proc compile*(tokens: seq[string]): seq[float] =
  ## Compiles tokenized code to NIA bytecode.
  ## tokens - Tokenized code.

  # TODO: Use array instead of case-of statement.

  result = @[]

  for token in tokens:
    case token:
    of "NOP": result.add(OP_NOP)
    # Stack.
    of "PUSH": result.add(OP_PUSH)
    of "POP": result.add(OP_POP)
    of "DUP": result.add(OP_DUP)
    of "SWAP": result.add(OP_SWAP)
    # Math.
    of "ADD": result.add(OP_ADD)
    of "SUB": result.add(OP_SUB)
    of "MUL": result.add(OP_MUL)
    of "DIV": result.add(OP_DIV)
    of "POW": result.add(OP_POW)
    of "INCR": result.add(OP_INCR)
    of "DECR": result.add(OP_DECR)
    # Special.
    of "JMP": result.add(OP_JMP)
    of "JNE": result.add(OP_JNE)
    of "PRINT": result.add(OP_PRINT)
    of "EXIT": result.add(OP_EXIT)
    else:
      try:
        result.add(token.parse_float())
      except:
        raise new_exception(UnknownTokenException, "Unknown token: \"" & token & "\".")