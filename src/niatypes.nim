type NIABREAKPOINT* = object
  pos*: int
  next*: ref NIABREAKPOINT
  name*: string

type NIATAREA* = object
  area*: HSlice[int, int]
  next*: ref NIATAREA
  name*: string

type NIADEBUG* = object
  breakpoints_head*: ref NIABREAKPOINT
  tareas_head*: ref NIATAREA

type NIAVM* = object
  memory*: seq[float]
  result*: int
  debug*: ref NIADEBUG

type UnknownOperationException* = object of Exception
type UnknownTokenException* = object of Exception