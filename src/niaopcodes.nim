# TODO: Add description for opcodes.

const OP_NOP* = 0

# Stack.
const OP_PUSH* = 1
const OP_POP* = 2
const OP_DUP* = 3
const OP_SWAP* = 4

# Math.
const OP_ADD* = 5
const OP_SUB* = 6
const OP_MUL* = 7
const OP_DIV* = 8
const OP_POW* = 9
const OP_INCR* = 10
const OP_DECR* = 11

# Special.
const OP_JMP* = 12
const OP_JNE* = 13
const OP_PRINT* = 14
const OP_EXIT* = 15
const OP_LAST* = OP_EXIT