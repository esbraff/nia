from os import exists_file, param_count, param_str
from strutils import ends_with, replace
from re import re, replace
import niacompile
import streams
import niavm

const RELEASE = false

when is_main_module:
  try:
    let argc = param_count()
    assert(argc >= 1, "Invalid arguments count.")

    let argument = param_str(1)

    case argument:
    of "--compile", "-c":
      assert(argc >= 2, "Filename expected.")

      let input_file = param_str(2)
      assert(input_file.ends_with(".nia"), ".nia file expected, got " & input_file & ".")

      if exists_file(input_file):
        let output_file = input_file.replace(".nia", ".niabin")

        let code = read_file(input_file)
        let tok = tokenize(code)
        let bcode = compile(tok)

        var stream = new_file_stream(output_file, fm_write)
        for i in bcode:
          stream.write(i)
        stream.close()
      else:
        assert(false, "Could not open file " & input_file & ".")
    of "--help", "-h":
      echo """
      Compilation:
        nia -c <filename>

        You can only compile .nia files!

      Execution:
        nia <filename>

        You can only execute .niabin files!
      """
    else:
      assert(argument.ends_with(".niabin"), ".niabin file expected, got " & argument & ".")

      var bcode: seq[float] = @[]

      var stream = new_file_stream(argument, fm_read)
      while not stream.at_end():
        bcode.add(float(stream.read_float64()))
      stream.close()

      var vm = newNIAVM(bcode, false)
      vm.interpret()
  except:
    when RELEASE:    
      let msg = re.replace(get_current_exception_msg(), re".* `.+` ")

      echo msg
    else:
      raise